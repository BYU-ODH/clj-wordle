(ns wordle-clj.views.main
  "Our main view (index) page, including introduction and an aggregation of the XML feeds of my four primary blogs."
  (:require [reagent.core :as r]
            [clojure.string :as str]
            [ajax.core :as ax :refer [GET]]
            [goog.string :as gstr]
            [goog.Uri.QueryData :as qd])
  )
(def NUM-TRIES (r/atom 6))
(def WORD-LENGTH (r/atom 5))
(defonce GUESSING (r/atom {:target ""
                           :guess-word-so-far []
                           :guessed-words []
                           :guessed-letters #{}}))

(def lowercase-alphabet
  #_(map String/fromCodePoint
       (range (.codePointAt \a 0) 
              (inc (.codePointAt \z 0)))))

(defn title-bar [] 
  [:h1 {:class "heading"} "Wordle!"])

(defn words-grid []
  [:h2 "Words grid"]
  (into [:div.words-grid 
         (for [nt (range @NUM-TRIES)] 
           (into [:div.row 
                  (for [wl (range @WORD-LENGTH)]
                    [:div.letter-box {:style {"border" "1px solid black"
                                              "width" "1em"
                                              "height" "1em"
                                              "display" "inline-block"
                                              "margin" "5px"
                                              "padding" "2px"}}])])
           )])
  )

(defn finalize-guess
  "Enter key submits a guess of `word-guessed`"
  [word-guessed]
  (js/alert (str "Finalizing your guess of " word-guessed))
  )

(defn letter-guess
  "Do the right thing if a letter is `guessed`"
  [guessed]
  ;; if enter, finalize the guess
  ;; backspace delete the most recent char
  ;; any letter, make that a new most recent guess of some index
  (cond
    (= "ENTER" (str/upper-case guessed)) (finalize-guess guessed)
    :otherwise (js/alert (str "You picked " guessed))

    ))

(defn keyboard-display []
  (let [qwerty ["qwertyuiop"
                "asdfghjkl"
                "zxcvbnm"]
        get-letters (fn [letter-seq] (seq letter-seq))
        [top-row home-row bottom-row] (map get-letters qwerty)
        key-styles {:style {"display" "inline-block"
                            "padding" "5px"
                            "border-style" "solid"
                            "border-color" "black"
                            "border-width" "1px"}}
        key-opts (fn [l] (assoc key-styles :on-click #(letter-guess l)))
        enter-key [:div.key (update (key-opts "Enter") :style merge {"font-size" "0.75em"}) "Enter"]        
        make-letter-key (fn [l] [:div.key (key-opts l) l])
        backspace-key (make-letter-key "␈")
        ]
    [:div.keyboard {:style {"text-align" "center"}}[:h2 "Keyboard"]
     (into [:div.top-row]
           (for [kbkey top-row]
             [make-letter-key kbkey]))
     (into [:div.home-row]
           (for [kbkey home-row]
             [make-letter-key kbkey]))
     (into [:div.bottom-row
            enter-key]
           (conj
            (list backspace-key)
            (for [kbkey bottom-row]
              [make-letter-key kbkey])))]))

(defn wordle-clj
  "Front-page view"
  []
  [:div
   [title-bar]   [words-grid]
   [keyboard-display]]) 

(comment
  (map String/fromCodePoint
       (range (.codePointAt \a 0) 
              (inc (.codePointAt \z 0)))))
