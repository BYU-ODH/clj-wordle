(ns wordle-clj.views.logic
  (:require [views.words :refer [choose-word dictionary]])
  (:gen-class))

(defn get-guess
  "Prompt for and get an accurate guess"
  []
  (println "Make a guess: ") ;; replace with guess prompt action
  (let [guess (clojure.string/trim (read-line))]
    (if (contains? dictionary guess)
      guess
      (do (println "Word not found in dictionary. Try again.")
        (get-guess)))))

(defn update-state
  "Updates the state of the game recursively"
  ([word guess old-state]
    (update-state word guess old-state ""))
  ([word guess old-state new-state]
    (if (empty? old-state)
      new-state
      (if (= (first word) (first guess))
        (recur (apply str (rest word)) (apply str (rest guess)) (apply str (rest old-state)) (str new-state (first word)))
        (recur (apply str (rest word)) (apply str (rest guess)) (apply str (rest old-state)) (str new-state (first old-state)))))))

(defn update-word
  "Prevents duplicating contained letters unless there are duplicates in the contains and the word"
  ([word letter]
    (update-word word letter ""))
  ([word letter new-word]
    (if (= (str (first word)) letter)
      (str new-word (apply str (rest word)))
      (update-word (apply str (rest word)) letter (str new-word (first word))))))

(defn update-contains
  "Update the contains variable of the game recursively"
  ([word guess]
    (update-contains word guess []))
  ([word guess new-contains]
    (if (empty? guess)
      new-contains
      (if (clojure.string/includes? word (str (first guess)))
        (recur (update-word word (str (first guess))) (apply str (rest guess)) (conj new-contains (str (first guess))))
        (recur word (apply str (rest guess)) new-contains)))))
    

(defn play-game
  "Recursive function for playing the wordle game"
  ([word guesses state]
    (play-game word guesses state []))
  ([word guesses state contains]
    (if (= state word)
      (println "You won! The word was" word) ;; replace with winning action
      (if (= guesses 0)
        (println "You lost! The word was" word) ;; replace with losing action
        (do (println state "contains:" contains) ;; replace with color highlights showing which letters are green, grey, yellow
          (let [guess (get-guess)]
            (let [new-state (update-state word guess state) new-contains (update-contains word guess)]
              (play-game word (- guesses 1) new-state new-contains))))))))


(defn -main
  "Main function for the wordle game"
  []
  (let [word (choose-word) guesses 6 state "-----"]
    (play-game word guesses state)    
))
