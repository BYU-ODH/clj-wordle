(ns wordle-clj.core
  (:require [reagent.core :as r]
            [wordle-clj.views.styles :as style]
            [wordle-clj.views.main :as main]))

(defn mount-components []
  (r/render-component [#'main/wordle-clj] (.getElementById js/document "odh-directory")))

(defn init! []
  (style/mount-style (style/wordle-clj))
  (mount-components))
